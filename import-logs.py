#!/usr/bin/env python3

import os
import sqlite3
from itertools import groupby
from xml.etree import ElementTree
from pathlib import Path


KOPETE_HISTORY_DB = os.path.expanduser('~/.kde4/share/apps/kopete/kopete_history.db')
TELEPATHY_LOG_PATH = os.path.expanduser('~/.local/share/TpLogger/logs')
TELEPATHY_PROTOCOLS = {
    'JabberProtocol': 'gabble_jabber',
    'ICQProtocol': 'haze_icq__3',
}


def read_kopete_logs():
    connection = sqlite3.connect(KOPETE_HISTORY_DB)
    connection.row_factory = sqlite3.Row

    cursor = connection.cursor()
    cursor.execute('SELECT * FROM history ORDER BY protocol, account, other_id, datetime')

    def group_key(row):
        return row['protocol'], row['account'], row['other_id'], row['datetime'][:10]
    return groupby(cursor, key=group_key)


def get_telepathy_account_id(kopete_protocol, kopete_account):
    return '{}_{}0'.format(
        TELEPATHY_PROTOCOLS[kopete_protocol],
        kopete_account.replace('@', '_40').replace('.', '_2e')
    )


def write_telepathy_logs(kopete_logs):
    for group, log in kopete_logs:
        kopete_protocol, kopete_account, partner_id, date = group
        telepathy_account = get_telepathy_account_id(kopete_protocol, kopete_account)
        write_telepathy_log(telepathy_account, partner_id, date, log)


def write_telepathy_log(telepathy_account, partner_id, date, log):
    treebuilder = ElementTree.TreeBuilder()
    treebuilder.start('log')
    treebuilder.data('\n')
    for row in log:
        outgoing = row['direction'] == '1'
        treebuilder.start('message', {
            'token': '',
            'message-token': '',
            'type': 'normal',
            'isuser': 'true' if outgoing else 'false',
            'id': row['me_id'] if outgoing else row['other_id'],
            'name': row['me_nick'] if outgoing else row['other_nick'],
            'time': row['datetime'].replace('-', ''),
        })
        treebuilder.data(row['message'])
        treebuilder.end('message')
        treebuilder.data('\n')
    tree = ElementTree.ElementTree(treebuilder.close())
    treebuilder.data('\n')

    log_dir = Path(TELEPATHY_LOG_PATH) / telepathy_account / partner_id
    if not log_dir.is_dir():
        log_dir.mkdir(parents=True)
    log_path = log_dir / (date.replace('-', '') + '.log')
    with log_path.open('wb') as log_f:
        log_f.write(b'<?xml version="1.0" encoding="utf-8"?>\n')
        log_f.write(b'<?xml-stylesheet type="text/xsl" href="log-store-xml.xsl"?>\n')
        tree.write(log_f, encoding='UTF-8')


def import_logs():
    kopete_logs = read_kopete_logs()
    write_telepathy_logs(kopete_logs)


if __name__ == '__main__':
    import_logs()
